const express = require('express');
const app = express();
app.use('/', express.static(__dirname));
const server = app.listen(3000, () => {
    require('livereload').createServer().watch([
        '**/*.(html|css)',
        '!.vscode',
        '!node_modules'
    ]);
    if (process.platform === 'win32') {
        require('child_process').exec(`start http://localhost:${server.address().port}`);
    }
})
